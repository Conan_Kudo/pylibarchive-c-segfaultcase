#!/usr/bin/env python3
import argparse
import os
import libarchive

dst = os.getcwd()

parser = argparse.ArgumentParser(description="Test success and failure case with python-libarchive-c")
parser.add_argument("-v", "--verbose", dest="verbose", action="store_true")
progmode = parser.add_mutually_exclusive_group(required=True)
progmode.add_argument("-s", "--single-item-iteration", dest="multiextract", action="store_false")
progmode.add_argument("-m", "--multi-item-extract", dest="multiextract", action="store_true")

args = parser.parse_args()

with libarchive.file_reader("small-0.1-1.noarch.rpm") as rpm:
    rpm_files = []
    for rpm_file_entry in rpm:
        if rpm_file_entry.pathname.startswith('./'):
            rpm_dst_pathname = dst + rpm_file_entry.pathname[1:]
        else:
            rpm_dst_pathname = dst + '/' + rpm_file_entry.pathname
        rpm_file_entry.pathname = rpm_dst_pathname
        if args.verbose == True:
            if args.multiextract == False:
                print("Extracting: {}".format(rpm_file_entry.pathname))
            else:
                print("Queuing up to be extracted: {}".format(rpm_file_entry.pathname))
        rpm_files.append(rpm_file_entry)
        if args.multiextract == False:
            # Note that this contains a list of one item
            # This does not work with a single item, it *must* be an iterable list
            libarchive.extract.extract_entries([rpm_file_entry])
    if args.multiextract == True:
        if args.verbose == True:
            print("Extracting queued up files...")
        # Note that this contains all the items in a single list
        # This should extract all the items with one call
        libarchive.extract.extract_entries(rpm_files)

